package com.example.restapi;

public class User {
    private String name,email,passwd,mob;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public User(String name, String email, String passwd, String mob) {
        this.name = name;
        this.email = email;
        this.passwd = passwd;
        this.mob = mob;
    }
}
