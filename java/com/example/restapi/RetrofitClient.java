package com.example.restapi;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private String BASE_URL=  "http://192.168.43.1/ ";
    private RetrofitClient retrofitClient;
    private Retrofit retrofit;
    private RetrofitClient()
    {



        retrofit = new Retrofit.Builder()
                .baseUrl("https://reqres.in")
                .addConverterFactory(GsonConverterFactory.create())
                .build();



        return retrofit;
    }
}
